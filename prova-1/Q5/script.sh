#!/bin/bash

read a b c d

mkdir $a $b $c $d

touch $a/README.md
date +"%d-%m-%Y" >> $a/README.md

cp $a/README.md $b/
cp $a/README.md $c/
cp $a/README.md $d/
